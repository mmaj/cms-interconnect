var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var app = null;
var config = null;

module.exports = function (_app, _config) {
    config = _config;
    app = _app;

    app.use(bodyParser.json());

    // Public CMS routes
    app.get('/cms/page/:path', getPageByRoute);

    // Internal CMS routes
    app.post('/cms/deployRoutes', deployRoutes);
    app.post('/cms/deployPages', deployPages);
    app.post('/cms/deployResources', deployResources);

    // Check for 404
    app.use(function (req, res, next) {
        findPageByRoute(req.path.slice(1), function (page, status, message) {
            if (status === 404) {
                // Next middleware should handle 404
                next();
            } else if (status === 200) {
                config.renderIndex(req, res);
            }
        });
    });

    return module.exports;
};

function getPageByRoute (req, res, next) {
    var route = req.params.path;
    var pagesPath =  filePath = path.join(__dirname, '../..', config.resourceDir, 'pages.json');

    fs.exists(pagesPath, function (exists) {
        if (exists) {
            var pages = fs.readFileSync(pagesPath, 'utf8');
            pages = JSON.parse(pages);
            var page = pages[route];
            if (page) {
                res.json(page);
            } else {
                res.status(404).send('There is no published page mapped to specified route');
            }
        } else {
            res.status(404).send('There are no pages published');
        }
    });
}

function findPageByRoute (_path, callback) {
    var pagesPath =  filePath = path.join(__dirname, '../..', config.resourceDir, 'pages.json');

    fs.exists(pagesPath, function (exists) {
        if (exists) {
            var pages = fs.readFileSync(pagesPath, 'utf8');
            pages = JSON.parse(pages);
            var page = pages[_path];
            if (page) {
                callback(page, 200);
            } else {
                callback(null, 404, 'There is no published page mapped to specified route');
            }
        } else {
            callback(null, 404, ('There are no pages published'));
        }
    });
}

function accessControl (req, res, success) {
	var ip = req.ip || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
    if (config && config.cmsServer === ip) {
    	success();
    } else {
    	res.status(401).send('You are not authorized to perform this operation.');
    }
}

function deployRoutes (req, res) {
	accessControl(req, res, function () {
        if (req.body) {
            var filePath = path.join(__dirname, '../..', config.resourceDir, 'routes.json');
            fs.writeFile(filePath, JSON.stringify(req.body), function (err) {
                if (err) {
                    console.log(err);
                    res.status(500).send(err);
                } else {
                    res.status(200).send();;
                }
            });
        } else {
            res.status(400).send('No body received');
        }
	});
}

function deployPages (req, res) {
    accessControl(req, res, function () {
        if (req.body) {
            var filePath = path.join(__dirname, '../..', config.resourceDir, 'pages.json');
            fs.writeFile(filePath, JSON.stringify(req.body), function (err) {
                if (err) {
                    console.log(err);
                    res.status(500).send(err);
                } else {
                    res.status(200).send();;
                }
            });
        } else {
            res.status(400).send('No body received');
        }
    });
}

function deployResources (req, res) {
	accessControl(req, res, function () {
		
	});
}